# StackTracer

Horrible "debugger" to try to get some practice learning how a tool for getting stack traces on crashes could be written in practice. This is not portable.

How it roughly works: 
 
1. Forks and spawns child process to debug
2. Ptrace attaches, continues waiting for a signal to be raised
3. If signal is SIGFPE, SIGABRT, or SIGSEGV, attempts to unwind stack frame
4. Reads process memory map out, looking for the image base address and the stack address range
5. Reads program register states at time of crash for rip and rbp
6. Reads list of base pointers and the saved return address on the stack until a base pointer points outside of the stack range (this means the end has been reached)
7. Adjusts the return addresses by the image base address offset to point to the actual virtual address without PIE
8. Reads the debugged child elf symbols, looking for all functions
9. Searches through the funcion symbols to find the functions the return addresses fall in, if available
10. Prints out the signal caught and the stack trace of return addresses from the unwound stack, referring to them as an offset from a function symbol name if available

## Usage
`make`  
`./stacker [target program path]`  
Does not support passing arguments to the target program currently.

![Stacker target](/docs/stackertarget.png)

![Stacker dnh](/docs/stackerdnh.png)
