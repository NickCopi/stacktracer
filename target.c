#include <stdio.h>
#include <unistd.h>


int read_value(int input){
	return * ((int *) input);
}

int do_neat_thing(){
	int input = 10;
	int output = read_value(input);
}

int main(int argc, char *argv[]){
	printf("Target says hi!\n");
	sleep(2);
	do_neat_thing();
	printf("Target says bye!\n");

}
