#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/user.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gelf.h>
#include <libelf.h>
#include <fcntl.h> 



typedef struct stack_frame{
	unsigned long base_pointer;
	unsigned long return_address;
	struct stack_frame *next;

} stack_frame_t;



typedef struct {
	unsigned long stack_start;
	unsigned long stack_end;
	unsigned long image_base;
} proc_map_t;

/* I should build a balanced binary tree for these probably instead */
typedef struct func_symbol{
	unsigned long start;
	unsigned long end;
	char name[64];
	struct func_symbol *next;
} func_symbol_t;


// original code from https://github.com/mateuspinto/elf-symbol-table-reader -- it is wrong
void read_elf_functions(char *filename, func_symbol_t *func_symbol) {
	Elf *elf;
	Elf_Scn *scn = NULL;
	GElf_Shdr shdr;
	Elf_Data *data;
	GElf_Sym sym;
	int fd, ii, count;
	func_symbol_t *last_symbol = func_symbol;

	elf_version(EV_CURRENT);

	fd = open(filename, O_RDONLY);
	elf = elf_begin(fd, ELF_C_READ, NULL);

	while ((scn = elf_nextscn(elf, scn)) != NULL) {
		gelf_getshdr(scn, &shdr);
		if (shdr.sh_type == SHT_SYMTAB) {
			/* found a symbol table, go print it. */
			break;
		}
	}

	data = elf_getdata(scn, NULL);
	count = shdr.sh_size / shdr.sh_entsize;

	/* print the symbol names */
	for (ii = 0; ii < count; ++ii) {
		gelf_getsym(data, ii, &sym);
		if(!sym.st_size) continue;
		func_symbol->start = sym.st_value;
		func_symbol->end = sym.st_value + sym.st_size;
		sprintf(func_symbol->name, "%.63s", elf_strptr(elf, shdr.sh_link, sym.st_name));
		func_symbol->next = malloc(sizeof(func_symbol_t));
		last_symbol = func_symbol;
		func_symbol = func_symbol->next;
	}
	free(func_symbol);
	last_symbol->next = NULL;
	elf_end(elf);
	close(fd);

}

pid_t spawn_child(char *child_argv[]){
	pid_t child_pid;
	if(!(child_pid = fork())){
		//ptrace(PTRACE_TRACEME, 0, NULL, NULL);
		execvp(child_argv[0], child_argv);
	}
	return child_pid;
}


void read_proc_map(pid_t child_pid, proc_map_t *proc_map){
	FILE *fp;
	int i = 0;
	char file_name[64] = {'\0'};
	char buf[4096] = {'\0'};
	sprintf(file_name, "/proc/%d/maps", child_pid); 
	fp = fopen(file_name, "r");
	while (fgets(buf, sizeof(buf), fp)) {
		if(!i)
			sscanf(buf, "%lx", &proc_map->image_base);
		//printf(buf);
		if(strstr(buf,"[stack]"))
			sscanf(buf, "%lx-%lx", &proc_map->stack_start, &proc_map->stack_end);

		i++;
	}
	//printf("image base %lx, stack range %lx-%lx\n", proc_map->image_base, proc_map->stack_start, proc_map->stack_end);
	fclose(fp);

}

void print_stack(stack_frame_t *stack_frame){
	if(!stack_frame) return;
	printf("Base address %lx. Return address %lx\n", stack_frame->base_pointer, stack_frame->return_address);
	print_stack(stack_frame->next);
}

void print_func_symbols(func_symbol_t *func_symbol){
	if(!func_symbol) return;
	printf("Func start %lx. Func end %lx Func name %s\n",func_symbol->start, func_symbol->end, func_symbol->name);
	print_func_symbols(func_symbol->next);
}


void unwind_frames(pid_t child_pid, stack_frame_t *stack_frame){
	proc_map_t proc_map = {};
	unsigned long last_base;
	struct user_regs_struct regs;
	read_proc_map(child_pid, &proc_map);
	ptrace(PTRACE_GETREGS, child_pid, NULL, &regs);
	//printf("rbp: %lx, rip %lx\n", regs.rbp, regs.rip);
	//printf("Instruction pointer: %x\n", regs.rip - proc_map.image_base);
	stack_frame->return_address = regs.rip - proc_map.image_base;
	stack_frame->base_pointer = regs.rbp;
	while(1){
		stack_frame->next = malloc(sizeof(stack_frame_t));	
		stack_frame->next->base_pointer = ptrace(PTRACE_PEEKDATA, child_pid, stack_frame->base_pointer, NULL);
		stack_frame->next->return_address = ptrace(PTRACE_PEEKDATA, child_pid, stack_frame->base_pointer + sizeof(long), NULL) - proc_map.image_base;
		if(stack_frame->next->base_pointer < proc_map.stack_start || stack_frame->next->base_pointer > proc_map.stack_end){
			free(stack_frame->next);
			stack_frame->next = NULL;
			break;
		}
		stack_frame = stack_frame->next;
	}
}

func_symbol_t *find_func_symbol(func_symbol_t *func_symbol, unsigned long return_address){
	/* Awful linear search. Instead these symbols should be loaded into a balanced binary tree. Oh well.*/
	while(func_symbol){
		if(func_symbol->start <= return_address && func_symbol->end >= return_address)
			return func_symbol;
		func_symbol = func_symbol->next;
	}
	return NULL;
}


void print_stack_trace(stack_frame_t *stack, func_symbol_t *func_symbol){
	stack_frame_t *stack_frame = stack;	
	func_symbol_t *current_symbol;
	while(stack_frame){
		current_symbol = find_func_symbol(func_symbol, stack_frame->return_address);
		if(current_symbol){
			printf("%s+0x%x\n", current_symbol->name, stack_frame->return_address - current_symbol->start);
		} else {
			printf("0x%.8lx\n", stack_frame->return_address);
		}
		stack_frame = stack_frame->next;
	}
}

int main(int argc, char *argv[]){
	pid_t child_pid, wpid;
	int status;
	stack_frame_t *stack;
	func_symbol_t *func_symbol;
	char *child_argv[] = {
		"./target"
	};
	if(argc > 1)
		child_argv[0] = argv[1];
	child_pid = spawn_child(child_argv);
	ptrace(PTRACE_ATTACH, child_pid, NULL, NULL);
	wpid = waitpid(child_pid, &status, WUNTRACED);
	ptrace(PTRACE_CONT, child_pid, NULL, NULL);
	while (1) {
		wpid = waitpid(child_pid, &status, 0);
		//printf("status %d %d\n", status, WIFSIGNALED(status));
		if (WIFEXITED(status)) {
			printf("Child process exited normally\n");
			break;
		} else if (WIFSTOPPED(status)) {
			int signal = WSTOPSIG(status);
			if (signal == SIGFPE || signal == SIGABRT || signal == SIGSEGV) {
				//printf("Desired signal caught: %d\n", signal);
				stack = malloc(sizeof(stack_frame_t));
				unwind_frames(child_pid, stack);
				//print_stack(stack);
				func_symbol = malloc(sizeof(func_symbol_t));
				read_elf_functions(child_argv[0], func_symbol);
				//print_func_symbols(func_symbol);
				printf("\n%s\n", strsignal(signal));
				print_stack_trace(stack, func_symbol);
				break;
			}
			ptrace(PTRACE_CONT, child_pid, NULL, NULL);
		} else {
			ptrace(PTRACE_CONT, child_pid, NULL, NULL);
		}
	}
}
